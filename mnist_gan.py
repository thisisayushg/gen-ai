import torch
import torchvision

from torch import nn
from torchvision import datasets
import numpy

from torch.optim import Adam
from torch.utils import data

from torch.utils.tensorboard import SummaryWriter


class Discriminator(nn.Module):
    def __init__(self, input_dim):
        super().__init__()
        self.disc = nn.Sequential(
            nn.Linear(input_dim, 256),
            nn.ReLU(0.01),
            nn.Linear(256, 128),
            nn.ReLU(0.01),
            nn.Linear(128, 64),
            nn.ReLU(0.01),
            nn.Linear(64, 32),
            nn.ReLU(0.01),
            nn.Linear(32, 1),
            nn.Sigmoid(),
        )

    def forward(self, x):
        return self.disc(x)


class Generator(nn.Module):
    def __init__(self, latent_dim, output_dim):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(latent_dim, 128),
            nn.ReLU(),
            nn.BatchNorm1d(128),
            nn.Linear(128, 256),
            nn.ReLU(),
            nn.BatchNorm1d(256),
            nn.Linear(256, 512),
            nn.ReLU(),
            nn.BatchNorm1d(512),
            nn.Linear(512, output_dim),
            nn.Tanh(),
        )

    def forward(self, x):
        return self.model(x)


generator = Generator(latent_dim=64, output_dim=784)
discriminator = Discriminator(input_dim=784)

# generator_learnable_params = [params for params in generator.parameters() if params.requires_grad]
generator_learnable_params = generator.parameters()
# discriminator_learnable_params = [params for params in discriminator.parameters() if params.requires_grad]
discriminator_learnable_params = discriminator.parameters()

gen_optim = Adam(generator_learnable_params, lr=3e-4)
disc_optim = Adam(discriminator_learnable_params, lr=3e-4)

criterion = nn.BCELoss()

BATCH_SIZE = 32
LATENT_DIM = 64
writer_fake = SummaryWriter("logs/fake")
writer_real = SummaryWriter("logs/real")
step = 0
fixed_noise = torch.randn((BATCH_SIZE, LATENT_DIM))

transforms = [torchvision.transforms.ToTensor(), torchvision.transforms.Normalize((0.5,), (0.5,))]
mnist_train = datasets.MNIST(
    root="data", train=True, download=True, transform=torchvision.transforms.Compose(transforms)
)
mnist_val = datasets.MNIST(
    root="data", train=False, download=True, transform=torchvision.transforms.Compose(transforms)
)


train_dataloader = data.DataLoader(mnist_train, batch_size=BATCH_SIZE, shuffle=True)
val_dataloader = data.DataLoader(mnist_val, batch_size=BATCH_SIZE, shuffle=True)

# imgs, _ = next(iter(train_dataloader))
# imgs = imgs.reshape((BATCH_SIZE, -1))
# noise = torch.randn(BATCH_SIZE, LATENT_DIM)
# fake_images = generator(noise)

# predicted_label_for_fake_image = discriminator(imgs)
# predicted_label_for_fake_image

print(f"Total batches: {len(train_dataloader)}")
device = torch.device("mps" if torch.backends.mps.is_available() else "cpu")
print(f"Device found: {device}")
generator = generator.to(device)
discriminator = discriminator.to(device)

for epoch in range(150):
    for idx, (imgs, _) in enumerate(train_dataloader):
        noise = torch.randn(BATCH_SIZE, LATENT_DIM).to(device)

        imgs = imgs.reshape((BATCH_SIZE, -1)).to(device)
        fake_images = generator(noise).to(device)

        # Discriminator Training
        # Discriminator is trained to tell the probability of an image being classisied as "real"
        # D(input) = probability of an input being classified as "real"
        # Objective function = maximize D(real) and maximize (1 - D(fake))
        #                    = maximize [log(D(real)) + log((1 - D(fake)))]
        #                    = - minimize [log(D(real)) + log(1 - D(fake))]
        #                    = - minimize [1 * log(D(real)) + 1 * log(1 - D(fake))]
        #                    = - min [(1 - 1) * (1 - log(D(real))) + 1 * log(D(real))] + [ (1- 0) * log(1 - D(fake)) + 0 * log(D(fake))]
        #                    = - min [(1 - real_label) * (1 - log(D(real))) + real_label * log(D(real))] + [(1 - fake_label) * log(1 - D(fake)) + fake_label * log(D(fake))]
        #
        # Here, D(real) = predicted label for real image and D(fake) = predicted label for fake image
        #                    = - min BinaryCrossEntropy(real_label, predicted_label_for_real_image) + BinaryCrossEntropy(fake_label, predicted_label_for_fake_image)
        predicted_label_for_fake_image = discriminator(fake_images).to(device)  # .view(-1)
        # print(predicted_label_for_fake_image)
        predicted_label_for_real_image = discriminator(imgs).to(device)  # .view(-1)

        # Create tensor of ones with same shape as predicted_label_for_real_image. Ones since real image is represented as 1
        real_image_targets = torch.ones_like(predicted_label_for_real_image).to(device)

        # Create tensor of ones with same shape as predicted_label_for_fake_image. Ones since fake image is represented as 0
        fake_image_targets = torch.zeros_like(predicted_label_for_fake_image).to(device)

        loss_d = criterion(predicted_label_for_real_image, real_image_targets) + criterion(
            predicted_label_for_fake_image, fake_image_targets
        )
        # #        \----------------------------------------------------------/.   \___________________________________________________________/
        # #            Loss when real image is fed                                            Loss when fake image is fed
        # print(f'disc losses: {criterion(real_image_targets, predicted_label_for_real_image)} / {criterion(fake_image_targets, predicted_label_for_fake_image)}')
        # print(f'disc losses: {criterion(predicted_label_for_real_image, real_image_targets)} / {criterion(predicted_label_for_fake_image, fake_image_targets)}')

        disc_optim.zero_grad()
        loss_d.backward(retain_graph=True)
        disc_optim.step()

        # Generator Training
        # Discriminator is trained to tell the probability of an image being classisied as "real"
        # D(input) = probability of an input being classified as "real"
        # Generator should be trained to maximize D(fake) i.e.
        # Generator should be trained to maximize the probability of a fake image being classified as "real"
        # Objective function = maximize D(fake)
        # Taking log of D(fake) since D(fake) is bound in 0 to 1
        # Need to include negative sign since log(x) when x<1 is negative
        #                    = - min log(D(fake))
        #                    = - min [1 * log(D(fake)) + (1-1) * log(1 - D(fake))]
        # real_label = 1 and fake_label = 0
        # i.e. Objective fn  = - min [real_label * log(D(fake)) + (1 - real_label) * log(1 - D(fake))]
        # i.e. Objective fn  = BinaryCrossEntropy(real_label, D(fake))

        predicted_label_for_fake_image = discriminator(fake_images).to(device)
        # print(predicted_label_for_fake_image)

        loss_g = criterion(predicted_label_for_fake_image, real_image_targets)

        gen_optim.zero_grad()
        loss_g.backward()
        gen_optim.step()

        # print(f'Epoch {epoch}: Batch {idx+1}, Discriminator loss = {loss_d:.4f}, generator loss = {loss_g:.4f}')

        if idx == 0:
            print(
                f"Epoch [{epoch}/{epoch}] Batch {idx}/{len(train_dataloader)} \
                      Loss D: {loss_d:.4f}, loss G: {loss_g:.4f}"
            )

            with torch.no_grad():
                fake = generator(fixed_noise.to(device)).reshape(-1, 1, 28, 28)
                data = imgs.reshape(-1, 1, 28, 28)
                img_grid_fake = torchvision.utils.make_grid(fake, normalize=True)
                img_grid_real = torchvision.utils.make_grid(data, normalize=True)

                writer_fake.add_image("Mnist Fake Images", img_grid_fake, global_step=step)
                writer_real.add_image("Mnist Real Images", img_grid_real, global_step=step)
                step += 1
